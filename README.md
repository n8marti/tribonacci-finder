# Tribonacci Finder

In response to ["Can You Make it to 2023?"](https://fivethirtyeight.com/features/can-you-make-it-to-2023/) from FiveThirtyEight:

>Many tribonacci sequences include the number 2023. For example, if you start with 23, 1000 and 1000, then the very next term will be 2023. Your challenge is to find starting whole numbers a, b and c so that 2023 is somewhere in their tribonacci sequence, a ≤ b ≤ c, and the sum a + b + c is as small as possible.

My son and I worked on this script together to find the solution (1, 1, 6):

```
$ ./g/tribonacci-finder/tribonacci-finder.py
[------------------------------------------------]
first = [1, 1, 6], last = [325, 598, 1100], next = 2023
```

Our strategy was a lazy one: set a max value for each of a, b, and c (100), then loop through each possible 3-digit combination (a, b, c) where a <= b <= c. If the next tribonacci value (a + b + c) <= 2023, then shift the values so that (b, c, next) becomes the new (a, b, c). If the next value equals 2023, then record it along with the three starting values for (a, b, c), then keep trying. Among all valid starting value sets, the one with the lowest initial sum is returned.

My son then suggested we modify the script so that the goal to shoot for can be passed as an argument. So then we tried this:

```
$ /g/tribonacci-finder/tribonacci-finder.py 500 112611
[-------------------------------------------------]
first = [12, 13, 206], last = [18098, 33291, 61222], next = 112611
```
