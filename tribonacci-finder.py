#!/usr/bin/env python3

import sys

# Set max integer value.
sum_max = 100
if len(sys.argv) > 1:
    sum_max = int(sys.argv[1])

# Set goal value.
goal = 2023
if len(sys.argv) > 2:
    goal = int(sys.argv[2])

# Calculate progress bar step length.
total_combos = sum_max ** 3
progress_len = 50
progress_step = int(total_combos / progress_len)

# Initialize progress bar.
print(f"[{' ' * (progress_len-2)}]", end='', flush=True)
print('\b' * (progress_len-1), end='')

tried_combos = 0
valid = {}
for a in range(1, sum_max):
    for b in range(1, sum_max):
        for c in range(1, sum_max):
            # Show progress.
            tried_combos += 1
            if tried_combos % progress_step == 0:
                print('-', end='', flush=True)
            if not a <= b <= c:
                continue
            trib = [a, b, c]
            next = sum(trib)
            while next <= goal:
                trib = trib[1:] + [next]
                next = sum(trib)
                if next == goal:
                    val = sum([a, b, c])
                    if not valid.get(val):
                        valid[val] = [[a, b, c], trib, next]
                    else:
                        valid.get(val).append([[a, b, c], trib, next])

print("]")
sorted_keys = [k for k in valid.keys()]
sorted_keys.sort()
if not sorted_keys:
    exit()
data = valid.get(sorted_keys[0])
print(f"first = {data[0]}, last = {data[1]}, next = {data[2]}")
